import ko from 'knockout';
import LikeOrDislike from './Components/LikeOrDislike'
import ProductViewModel from './ViewModels/ProductViewModel'

//https://lorefnon.me/2015/08/24/smarter-knockout-applications-with-es-next.html

ko.applyBindings(new ProductViewModel(), document.getElementById('root'));
