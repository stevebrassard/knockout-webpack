import ko from 'knockout'
//import toastr from 'toastr'
//import '../../node_modules/toastr/build/toastr.css'
import likeOrDislikeTemplate from '../Templates/like-or-dislike.html';
import '../Styles/LikeOrDislike.css'

class LikeWidgetViewModel {
  constructor(params) {
    this.chosenValue = params.value;
  }

  like() {
    this.chosenValue('like');
    //toastr.success("You liked it!")
  }

  dislike() {
    this.chosenValue('dislike');
    //toastr.error("You disliked it!");
  }
}

ko.components.register('like-or-dislike', {
  viewModel: LikeWidgetViewModel,
  template: likeOrDislikeTemplate
});