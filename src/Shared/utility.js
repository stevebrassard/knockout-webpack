
//This is an example of having multiple utility functions that can be reused in other code files
// to import these functions in other code files, do the following:
// import {debounce} from './utility'
// import {openNewWindow} from './utility'
// import {debounce, openNewWindow} from './utility.js'

// http://davidwalsh.name/javascript-debounce-function
export function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

export function openNewWindow(location) {
	window.open(location, "_blank");
};


