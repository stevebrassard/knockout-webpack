import ko from 'knockout'
import Product from '../BusinessObjects/Product'

export default class ProductsViewModel {
    constructor() {
        this.products = ko.observableArray();
        this.likedCount = ko.pureComputed(() => this.products().filter(p => p.userRating() == 'like').length);
        this.dislikedCount = ko.pureComputed(() => this.products().filter(p => p.userRating() == 'dislike').length);
    }

    addProduct() {
        const name = `Product ${this.products().length + 1}`;
        this.products.push(new Product(name));
    }
}