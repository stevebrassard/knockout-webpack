import ko from 'knockout';

export default class Product {
    constructor(name, rating = null) {
        this.name = name;
        this.userRating = ko.observable(rating);
    }
}