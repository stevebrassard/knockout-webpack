## Knockout-Webpack

A simple example of using knockout and webpack together

-----------------

To get started, open a terminal from the cloned folder and run:

npm install

then, 

npm run dev

Open another terminal (from the same cloned folder) and run:

npm run serve

To optimize your bundle.js output, run the following command:

npm run prod